# Container scanning

Demonstrate use of [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/) using Clair for known vulnerabilities during build time.

For examples regarding Docker-build jobs, please refer to <https://gitlab.cern.ch/gitlabci-examples/build_docker_image>.

## Limitations

On <https://gitlab.cern.ch> we have an Enterprise License at the Starter level, meaning that GitLab will not be able to show the scanning results on the Web Interface as <https://docs.gitlab.com/ee/user/application_security/container_scanning/> states, **but users are able to check the report results on the corresponding CI job output, as in [this example](https://gitlab.cern.ch/gitlabci-examples/container_scanning/-/jobs/4196586).**

**Please note that the vulnerability checks are done on build time. You will need to rebuild your Docker images and rescan periodically to ensure your image is not vulnerable.**
